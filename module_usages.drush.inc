<?php

/**
 * @implemnts hook_drush_command()
 */
function modules_usages_drush_command() {
  $items = array();

  $items['module-usage-status'] = array(
    'description' => "Create a status report of modules and there usages count as CSV export.",
    'aliases' => array('mus'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );

  return $items;
}

/**
 * Get a list of modules from drupal with all the current info
 *
 * drush pm-list --type=Module --status=enabled
 */
function moduke_usage_drush_get_modules() {
  $drupal_version = (int) VERSION;
  if ($drupal_version >= 7 && $drupal_version < 8) {
    $list_modules_function = 'system_rebuild_module_data';
  }
  else {
    if ($drupal_version >= 6 && $drupal_version < 7) {
      $list_modules_function = 'module_rebuild_cache';
    }
    else {
      watchdog(WATCHDOG_ERROR, t('You are not using drupal 6 or 7 of drupal'));
      RETURN NULL;
    }
  }
  if (!function_exists($list_modules_function)) {
    watchdog(WATCHDOG_ERROR, t("Unable to find the function $list_modules_function "));
    return NULL;
  }
  return $list_modules_function();
}

/**
 * @param $project
 * @param $version
 * @return array
 */
function model_usage_get_info_by_name_and_version ($project, $version) {

  $url = 'https://drupal.org/project/usage/' . $project;
  $headers = array(
    "Accept-Language: en-us",
    "User-Agent: Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)",
    "Connection: Keep-Alive",
    "Cache-Control: no-cache",
  );
  $referer = 'http://www.google.com/search';

  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_REFERER, $referer);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $html = curl_exec($ch);

  $document = new DOMDocument();
  $document->loadHTML($html);

  $xpath = new DOMXPath($document);
  $xpath = new DOMXPath($document);
  $project_version_nodes       = $xpath->query('//table[@id="project-usage-project-releases"]/tbody/tr/td[@class="active"]/a');
  $project_version_usage_nodes = $xpath->query('//table[@id="project-usage-project-releases"]/tbody/tr/td[@class="project-usage-numbers"][1]');


  $info = array();

  foreach ($project_version_nodes as $item_nr => $node) {
    /** @var DOMNode $node */
    $project_version = trim($node->textContent);

    $info['latest_version'] = $project_version;

    $corresponding_version_usage_node = $project_version_usage_nodes->item($item_nr);
    $usage += (int)trim($corresponding_version_usage_node->textContent);
    $info['latest_version_usages'] = (int)trim($corresponding_version_usage_node->textContent);

    if ($project_version !== $version) {
      continue;
    }

    if (!isset($usages['usages'])) {
      $corresponding_version_usage_node = $project_version_usage_nodes->item($item_nr);
      $usage += (int)trim($corresponding_version_usage_node->textContent);
      $info['usages'] = (int)trim($corresponding_version_usage_node->textContent);
    }
  }

  return $info;
}

/**
 * @param $values_grid
 * @return string
 */
function modules_usages_to_csv($values_grid) {
  $outputBuffer = fopen("php://output", 'w');
  if (count($values_grid) !== 0) {
    fputcsv($outputBuffer, array_keys(reset($values_grid)));
  }
  foreach($values_grid as $val) {
    fputcsv($outputBuffer, $val);
  }
  fclose($outputBuffer);
  return $outputBuffer;
}

/**
 * @param string $filling
 * @return string
 */
function drush_modules_usages_status($filling = 'ascii') {
  $modules = moduke_usage_drush_get_modules();
  if ($modules === NULL) {
    return t("Modules list could not load.");
  }

  $value_grid = array();
  foreach ($modules as $name => $module) {
    if ($module->info['package'] === 'Core' || $module->info['project']  === 'drupal') {
      continue;
    }

    $info =  model_usage_get_info_by_name_and_version($name, $module->info['version']);

    $version = $module->info['version'];

    $results = array();
    if (preg_match('/-(?P<version>.*)/', $version, $results) === 1) {
      $version = $results['version'];
    }
    if (preg_match('/[\d\.]+/', $version) === 1) {
      $status = 'Stable';
    } else if (strpos($version, 'beta') !== false) {
      $status = 'Beta';
    } else if (strpos($version, 'alpha') !== false) {
      $status = 'Alpha';
    } else if (strpos($version, 'dev') !== false) {
      $status = 'Dev';
    } else if (strpos($version, 'unstable') !== false) {
      $status = 'Unstable';
    } else if (strpos($version, 'rc') !== false) {
      $status = 'RC';
    } else {
      $status = 'Unknown';
    }

    $row = array(
      'name' => $name,
      'enabled' => (int)$module->status ? 'enabled' : 'installed',
      'php' => $module->info['php'],
      'status' => $status,
      'version' => $module->info['version'],
      'usages' => isset($info['usages']) ? $info['usages'] : 0,
      'latest_version' => isset($info['latest_version']) ? $info['latest_version'] : '',
      'latest_version_usages' => isset($info['latest_version_usages']) ? $info['latest_version_usages'] : 0
    );

    $value_grid[] = $row;
  }

  return modules_usages_to_csv($value_grid);
}
echo '<pre>';
echo drush_modules_usages_status();
die;